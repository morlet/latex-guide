﻿Tuto-Latex
==========

Souvent présenté comme incontournable pour rédiger rapports et articles, \LaTeX{} n'est cependant pas enseigné comme peut l'être la programmation.

Bien qu'il existe certaines ressources en ligne, aucune d'elles n'a donné le sentiment d'être à la fois pratique et complète : souvent, une fois les bases posées, le lecteur est laissé à lui-même.

Ce document a pour but de présenter de façon assez complète les bases du langage de mise en forme de texte formaté \LaTeX{}, ainsi que certaines de ses fonctionnalités plus avancées qui pourront vous servir dans votre vie de scientifique.

SOMMAIRE (ET TO-DO LIST)
----------

Code :
[ ] : vide
[...] : en cours
[V] : fait
[/] : fait et relu (1ère passe)
[X] : fait et approuvé par l'OTL

1. Introduction
    * [/] LaTeX ?
    * [/] L'installation
    * [/] La compilation
2. LaTeX : Guide de survie
    * [/] Structure d’un fichier source : les bases
    * [/] Quelques notions sur la syntaxe LaTeX
    * [/] Construire son préambule
    * [/] L’environnement document et sa structure
    * [V~] Formater le texte
    * [/] Les listes
    * [/] Labels et Références
    * [/] Les flottants : images et tableaux
    * [/] Ecrire des mathématiques
3. LaTeX : Pour l'utilisateur avancé
    * [V] Personnalier la mise en page
    * [/] Tuner ses tableaux
    * [/] Gestion avancée des figures
    * [V] Mathématiques avancées
    * [/] SIUnitx : le paquet du physicien
    * [/] Des références d'articles avec BibTeX
    * [V] Gérer un gros document en le découpant en plusieurs parties
    * [V] Un soupçon de programmation LaTeX
4. Des graphiques vectoriels avec le duo PGF/TIKZ
    * [/] Introduction
    * [/] Des figures simples
    * [/] Styles, couleurs et décorations
    * [/] Représenter des fonctions et des données avec pgfplots
    * [/] Un soupçon de 3D
    * [/] Un peu de théorie des graphes
    * [/] Divers trucs et astuces
5. Inclure du code source avec listings
    * [ ]
6. Des présentations avec Beamer
    * [ ]

Annexes
    * [V] Un templace de fichier .tex pour un article
    * [...] Liste d'extensions utiles
    * [V] Les accents et caractères spéciaux
    * [ ] Lire les erreurs de compilation dans le .log

TODO
    * Section OTL / syntaxe française et anglaise ?
