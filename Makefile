########################## MAKEFILE LaTeX GUIDE #############################
# Constantes
SOURCE=latex-guide.tex
AUX=latex-guide.aux
EXEM=ex.aux
TIKZ=tikz/

# Règles
all: check-tikz compile

latexmk:
	latexmk --enable-write18 --interaction=nonstopmode --synctex=1 -pdf $(SOURCE)

compile:
	pdflatex --enable-write18 ${SOURCE}
	bibtex ${AUX}
	bibtex ${EXEM}
	pdflatex ${SOURCE}
	pdflatex ${SOURCE}

check-tikz:
	@test -d ${TIKZ} || mkdir ${TIKZ}

clean:
	@echo "Nettoyage des fichiers de compilation..."
	@rm -f *.aux *.log *.bbl *.blg *.toc *.out *.auxlock *.tmp *.fdb_latexmk *.fls
	@cd parts/ ; rm -f *.aux

mrproper: clean
	@echo "Nettoyage des fichiers générés..."
	@cd ${TIKZ} ; rm -f *
	@rm -f *.pdf
