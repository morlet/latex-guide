\part{Introduction}

Une petite introduction à ce qu'est \LaTeX{} et à comment il s'utilise.

\section{\LaTeX{} ?}

L'histoire commence aux alentours de 1977 auprès d'un certain Donald Ervin Knuth, professeur de mathématiques et d'informatique à l'université de Stanford. Dix ans auparavant, Knuth avait commencé la rédaction d'un ouvrage de référence pour la programmation intitulé \emph{The Art of Computer Programming}, et la première édition de son ouvrage avait été imprimée \og{}à l'ancienne\fg{}, avec une presse et des lettres de plomb ; le rendu avait alors beaucoup plus à son auteur. Cependant, les techniques d'impression étaient en pleine évolution et à l'époque qui nous intéresse, cette méthode d'impression n'était plus disponible pour la seconde édition du livre de Knuth. Elle avait été remplacée par la méthode de photocomposition, dont les résultats, surtout pour les formules mathématiques, irritèrent fortement l'auteur lorsqu'il reçut les premières épreuves de cette réédition.

Knuth s'intéressa alors de plus près à la typographie numérique, et décida de concevoir un programme capable de générer des mises en page propres et agréables, en particulier pour les formules mathématiques, et permettant d'obtenir le même rendu quel que soit l'ordinateur sur lequel on travaillait. Ainsi naquit le système de composition \TeX, dont le nom est issu de la première syllabe du mot grec \textgreek{t'eqnh} qui signifie art et science. Notez d'ailleurs que la prononciation de ce nom est précisément définie : dans son livre \emph{The \TeX book}, Knuth explique que l'on doit prononcer le `X' de \TeX{} comme un \textgreek{q} en grec, lequel se prononce [x] (comme le `j' en espagnol ou le `ch' en écossais (\textit{loch}) ou en allemand (\textit{ach})). Cependant, dans tous ses mots d'origine grecque le français a changé la prononciation originelle du \textgreek{q} en [k], aussi la bonne manière de prononcer le nom de ce système en français est [t\textipa{E}k]\footnote{Évitez donc en particulier l'horrible [t\textipa{E}ks] (comme dans \textit{texan}), qui ne vous attirera que railleries !}.

Dans sa forme pure, \TeX{} est un système d'utilisation difficile, extrêmement bas niveau et donc ésotérique pour le profane. Afin de faciliter son utilisation, un autre chercheur du nom de Leslie Lamport écrivit en 1983 une collection de macro-commandes autour de \TeX{} destinée à le rendre plus simple ; ce qui donna naissance à un système nommé \LaTeX{}\footnote{Abréviation de \textit{La}mport \textit{\TeX}, et qui se prononce donc [lat\textipa{E}k] en France et [lat\textipa{E}x] ailleurs ; mais surtout pas [lat\textipa{E}ks] !...}. Contrairement à des logiciels dits \og{}WYSIWYG\footnote{\emph{What You See Is What You Get}.}\fg{}, tels que \texttt{Word} ou \texttt{LibreOffice}, \TeX{} et \LaTeX{} séparent le contenu et le rendu, reposant sur des commandes (ou \emph{macros}) et nécessitant une étape de compilation. L'avantage apporté par cette approche par rapport aux autres est qu'elle permet à l'utilisateur de se concentrer sur le contenu de son document, les soucis de mise en page étant largement gérés par le système lui-même, tout en lui laissant suffisament de flexibilité pour pouvoir obtenir exactement ce qu'il veut.

Même s'il paraît \emph{a priori} plus compliqué à utiliser que les logiciels classiques, \LaTeX{} est très robuste : le rendu (via le format PDF) sera toujours le même (entre systèmes et entre générations), le compilateur est un logiciel libre et multi-plateforme, et un simple éditeur de texte suffit pour modifier la source d'un document (un fichier \verb|.tex|). À travers les commandes intégrées ou l'utilisation d'extensions, il est possible de produire tout type de document (article, mémoire, lettre) -- celui que vous avez devant les yeux compris\footnote{Il est même possible de faire un journal en \LaTeX{}, la preuve avec \emph{La Sauce}, le journal du campus de l'ENS Cachan.}. Par ailleurs, \LaTeX{} s'est peu à peu imposé comme un système incontournable dans le monde de la recherche scientifique, et la maîtrise de ce programme est une compétence très appréciée dans de nombreux domaines.

\vspace{\baselineskip}

\emph{Terminons cette introduction en signalant une dernière particularité à connaître quant aux noms} \TeX{} \emph{et} \LaTeX{} \emph{: lorsque l'on ne dispose pas des commandes} \verb|\TeX| \emph{et} \verb|\LaTeX| \emph{pour taper automatiquement les noms de ces systèmes, on écrit} \texttt{TeX} \emph{et} \texttt{LaTeX}\emph{, en prenant bien soin de mettre les lettres `a' et `e' en minuscule et le reste en majuscule. Ce petit détail typographique permet de distinguer ces systèmes d'autres logiciels avec des noms similaires, et aussi d'éviter de confondre ces noms avec le pseudonyme d'un humoriste, une matière séveuse naturelle ou un polymère servant à la fabrication de pneus et de combinaisons moulantes.}


\section{L'installation}

Parce que \LaTeX{} fait appel à des commandes, il faut compiler le fichier source pour obtenir le fichier de sortie. Deux possibilités se présentent alors à vous : 
\begin{itemize}
	\item utiliser un \emph{IDE}\footnote{Pour \textit{Integrated Development Environment}, ce qui en français donne \og environnement de développement intégré \fg{}.} tel que \verb=TeXmaker= ou \verb=TeXworks=, qui combine éditeur de texte et interface de compilation. Ils ont l'avantage d'automatiser la compilation, en gérant à votre place les appels au compilateur	;
	\item travailler de façon plus artisanale en utilisant votre éditeur de texte favori et en donnant les instructions de compilation dans un terminal (directement ou en utilisant l'outil \verb|latexmk|, voir au \ref{subsec:latexmk}).
\end{itemize}

Si vous n'êtes pas familier avec les environnements UNIX et/ou que les aspects techniques ne vous intéressent pas, nous vous conseillons d'utiliser un \emph{IDE}.

Peu importe la solution que vous choisissez, il faut installer séparément le compilateur. \verb|TeXlive| est la distribution la plus répandue, disponible pour tous les systèmes d'exploitation ; sous Windows, il existe également \verb=MikTex=.

Installer la totalité de la distribution est assez coûteux en mémoire (supérieur à \SI{1}{\giga\octet}), mais c'est le meilleur moyen d'être sûr de ne manquer de rien. Vous pouvez aussi ne télécharger que la distribution de base et ajouter les extensions au fur et à mesure de vos besoins.  \verb|MikTeX| propose également un système d'installation des extensions à la volée\footnote{C'est-à-dire au moment de la compilation, si vous en avez besoin.}, mais ce système nécessite une connexion internet.

Il existe également des interfaces en ligne, qui compilent automatiquement : \texttt{Overleaf} et \texttt{ShareLaTeX}. Bien qu'elles permettent de collaborer rapidement en ligne et sans installation, elles montrent rapidement leurs limites. Utiliser \texttt{Git} (logiciel de gestion de versions), en parallèle de son installation locale, résout ce problème.

\section{La compilation}

Si vous utilisez un \textit{IDE} et que le côté technique ne vous intéresse pas vous pouvez vous passer de lire cette section.

Mais si vous désirez compiler votre document à la main (sans \textit{IDE}), comprendre comment utiliser des options de compilation et des fichiers externes (\texttt{.bib} par exemple), ou que vous êtes tout simplement curieux de voir comment se déroule la compilation, alors vous êtes au bon endroit ! 

Il y a deux moyens de compiler un document \LaTeX{}, avec chacun leur compilateur associé :
\begin{itemize}
	\item la méthode \og historique \fg{} qui utilise le compilateur \verb|latex| pour créer un document \texttt{.dvi}, que l'on peut ensuite convertir en \texttt{.pdf}
	\item la compilation directe en \texttt{.pdf}, qui utilise le compilateur \verb|pdflatex|
\end{itemize}

%\startaurel En cherchant les différence entre les deux voies de compilation je suis tombé sur une extension qui ne fonctionne qu'avec pdflatex : \verb|microtype|, qui fait de la microtypographie, elle adapte le texte pour éviter quasiment toutes les césures de fin de ligne et fait de la micro-gestion des ponctuations, histoire de pousser l'OTL à l'extrême \verb|^^| On devrait carrément faire une section OTL en fait ;) \closeaurel

La méthode historique semble néanmoins tomber peu à peu en désuétude. Elle était très utilisée dans les débuts du langage, mais  \verb|pdflatex| est maintenant beaucoup plus performant et produit des documents de meilleure qualité. La seule raison pour laquelle vous pouvez donc être amené à utiliser la compilation en \texttt{.dvi} est si vous êtes un adepte de l'extension \verb|pstricks|\footnote{Qui permet de faire du dessin vectoriel, comme Ti$k$Z.}.

Pour ces raisons nous ne parlerons dans ce document que de \verb|pdflatex|, et c'est désormais ce compilateur qui sera sous-entendu à chaque fois que sera mentionné l'opération de compilation.\\
Si vous utilisez un \textit{IDE} assurez-vous donc que c'est bien \verb|pdflatex| qui est utilisé (mais c'est normalement le cas par défaut si vous créez un \texttt{.pdf})



\subsection{Le compilateur et ses options}

L'appel au compilateur se fait en terminal UNIX (ou en ligne de commande Windows), avec la syntaxe habituelle \verb|pdflatex -option1 -option2 monfichier.tex|

Les options les plus utiles sont résumées dans le tableau suivant. Et pour plus de détails comme toujours lisez le manuel.

\begin{tabular}{|c|c|c|}
	\hline
	Option & Utilité & Commentaire\\
	\hline
	\hline
	\verb|-interaction=nonstopmode| & compile d'une traite sans & les \textit{IDE} utilisent cette \\
	& demander l'avis utilisateur & option par défaut \\
	\hline
	 & crée un fichier de synchronisation & il faut que votre éditeur  \\
	\verb|-synctex=1|& source-sortie, afin de relier ce & supporte cet effet, les \textit{IDE}\\
	& qui est vu dans l'éditeur &  le font sans problème, les éditeurs\\
	& au document \texttt{.pdf} & comme \textit{vim} ont besoin d'un \textit{plugin} \\
	\hline
	\verb|-verbose| & pour plus de détails dans la sortie & $\emptyset$ \\
	\hline
	\verb|-output-directory=dossier| & place le résultat de la & pour plus de propreté\\
	& compilation dans ce dossier & dans vos dossiers\\
	\hline
	 & autorise \verb=pdflatex= a envoyer & utile pour l'externalisation \\
	\verb|-shell-escape|& des commandes au terminal & des graphes Ti$k$Z \\
	& (écriture sur la sortie $18$) & \\
	\hline
	\verb|-draftmode| & compile sans inclure les images & accélère la compilation \\
	& et sans créer de \texttt{.pdf} &  \\
	\hline
\end{tabular}

\subsection{Le déroulement de la compilation : incidents et fichiers auxiliaires}

\label{subsec:erreurs}

Lors de la compilation, des erreurs peuvent survenir si vous avez fait une erreur (syntaxique ou logique)\footnote{Une bonne syntaxe \LaTeX{} est assez exigeante, ne vous étonnez pas si vous avez beaucoup d'erreurs, une grande partie d'entre elles sont sûrement inoffensives}. 

Si vous n'êtes pas en \verb|nonstopmode|, la compilation se mettra en pause et on vous proposera de résoudre cette erreur avant de continuer.\\
Si vous êtes en \verb|nonstopmode|, regardez attentivement votre document de sortie car une erreur a pu avoir lieu sans que vous ne l'ayez vue.

Les erreurs se classent en deux catégories : 
\begin{itemize}
	\item les \textit{avertissements} sont pour la plupart inoffensifs, et vous pouvez les ignorer dans un premier temps (pour y revenir s'ils vous posent problème) ;
	\item les \textit{erreurs fatales} vont causer un arrêt soudain de la compilation et ne pas produire de document.
\end{itemize}

Dans tous les cas les erreurs sont écrites dans un fichier texte portant l'extension \texttt{.log}, que vous pouvez consulter en cas de problème. Un \textit{IDE} est en général capable de lire automatiquement les \texttt{.log} et de vous signaler un problème.

L'annexe \ref{app:errors} présente des erreurs courantes et la façon d'y remédier, n'hésitez pas à la consulter en cas de problèmes.

Un avertissement mérite néanmoins d'être présenté dans cette section : \\
\og \texttt{LaTeX Warning: There were undefined references} \fg{}\\
Cet avertissement apparaît si vous avez utilisé un \verb|\ref{}| auquel ne correspond aucune étiquette (voir au \ref{subsec:label}), mais la petite subtilité est qu'il apparaîtra également si c'est la première fois que vous compilez depuis que vous avez créé l'étiquette\ldots

En effet le compilateur lit le fichier source linéairement et est donc parfaitement incapable de gérer des références croisées ! Pour contourner ce problème, il écrit les étiquette dans un fichier texte portant l'extension \texttt{.aux} (pour auxiliaire), et lorsqu'il rencontre une référence il cherche si une étiquette correspondante existe dans le \texttt{.aux}. C'est pour cette raison qu'une référence croisée n'apparaît jamais lors de sa première compilation et que deux compilations sont nécessaires ! 

Pour finir, notez que le compilateur va créer des fichiers secondaires (le \texttt{.log}, le \texttt{.aux}, et d'autres) lors de la compilation. Vous les trouverez peut-être inutiles et aurez envie de vous en débarrasser, mais ils ont de l'importance pour \verb|pdflatex| et les supprimer revient à tout devoir recompiler.

\subsection{Compiler la bibliographie}
\label{subsec:compbib}
La bibliographie nécessite plusieurs étapes afin d'être rendue proprement. Une fois que celle-ci est rédigée (votre fichier \texttt{.bib} est prêt et appelé dans le corps du document, cf. la section~\ref{sec:biblio}), il faut dans l'ordre :
\begin{itemize}
\item Compiler le fichier \texttt{.tex} avec \texttt{pdflatex} une première fois ;
\item Compiler le fichier \texttt{.aux} avec \texttt{bibtex} pour créer les références bibliographiques ;
\item Compiler le fichier \texttt{.tex} avec \texttt{pdflatex} deux fois\footnote{Pour éviter les problèmes dus aux références croisées mentionnés plus haut}.
\end{itemize}

Si vous utilisez un \textit{IDE}, il existe toujours un bouton, menu déroulant ou entrée qui permet d'appeler \texttt{bibtex} plutôt que \texttt{pdflatex} (voir l'image ci-dessous pour l'exemple de \texttt{Texmaker}). Il faut donc appeler \texttt{pdflatex} et \texttt{bibtex} dans le même ordre que ci-dessus pour obtenir le résultat désiré.

\begin{figure}[h]
\centering
\includegraphics[scale=1]{./images/bibtex_ide.png}
\caption{Menu déroulant permettant de choisir le compilateur appelé par \texttt{Texmaker}. Ces choix se retrouvent aussi dans le menu \texttt{Outils}.}
\end{figure}

\subsection{L'outil latexmk}

\label{subsec:latexmk}

Si vous n'utilisez pas d'\textit{IDE} mais que vous ne voulez pas tout compiler à la main à chaque fois, \verb|latexmk| est une excellente solution pour vous !

En effet, il s'agit d'un programme agissant à la manière d'un \verb|makefile|\footnote{Pour ceux qui ne connaissent pas, c'est une sorte de \og recette de cuisine \fg{} pour compiler un programme, ils sont très utiles aux programmeurs pour s'assurer que tout le monde compile de la même façon.} (d'où son nom), qui automatise la compilation en appelant \verb|pdflatex| et \verb|bibtex| exactement le bon nombre de fois\footnote{Contrairement à certains programmes qui se contentent de compiler deux fois car \og dans le doute ça marchera \fg{}.} afin d'obtenir le résultat désiré.

Pour l'utiliser en terminal il faut taper 
\begin{center}\verb|latexmk -pdf (--autres-options-pour-pdflatex) monfichier.tex|\end{center}

Il est également utilisé comme base par d'autres programmes comme le \textit{plugin} \verb|vimtex| de \verb|vim| (voir \url{https://github.com/lervag/vimtex}).


